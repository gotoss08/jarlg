using RLNET;

namespace AgentCorporation.Structs
{
    public struct Message
    {
        private RLColor _color;
        private string _textMessage;

        public Message(string textMessage)
        {
            _textMessage = textMessage;
            _color = RLColor.White;
        }

        public Message(string textMessage, RLColor color)
        {
            _textMessage = textMessage;
            _color = color;
        }

        public RLColor Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public string TextMessage
        {
            get { return _textMessage; }
            set { _textMessage = value; }
        }
    }
}