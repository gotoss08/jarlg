﻿using RLNET;

namespace AgentCorporation.Structs
{
    public struct Stat<T>
    {
        private RLColor _color;
        private string _name;
        private T _value;

        public Stat(T value)
        {
            _name = "no stat name";
            _color = RLColor.White;
            _value = value;
        }

        public Stat(string name, T value)
        {
            _name = name;
            _value = value;
            _color = RLColor.White;
        }

        public Stat(string name, T value, RLColor color)
        {
            _name = name;
            _value = value;
            _color = color;
        }

        public string GetName
        {
            get { return _name; }
            set { _name = value; }
        }

        public RLColor GetColor
        {
            get { return _color; }
            set { _color = value; }
        }

        public T GetValue
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}