﻿namespace AgentCorporation.Core
{
    public static class Fps
    {
        private static double _time, _frames;
        private static int _fps;

        public static int GetFps(double time)
        {
            _time += time;
            if (_time < 1.0)
            {
                _frames++;
                return _fps;
            }
            _fps = (int)_frames;
            _time = 0.0;
            _frames = 0.0;
            return _fps;
        }
    }
}