﻿using AgentCorporation.Interfaces;
using AgentCorporation.Structs;
using RLNET;
using RogueSharp;

namespace AgentCorporation.Core
{
    public class Actor : IDrawable, IActor
    {
        private Stat<int> _attack;
        private Stat<int> _attackChance;
        private Stat<int> _awareness;

        private RLColor _color;
        private Stat<int> _defense;
        private Stat<int> _defenseChance;
        private Stat<int> _gold;
        private Stat<int> _health;
        private Stat<int> _maxHealth;
        private Stat<string> _name;
        private Stat<int> _speed;
        private char _symbol;
        private int _x;
        private int _y;

        public Stat<string> Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Stat<int> Attack
        {
            get { return _attack; }
            set { _attack = value; }
        }

        public Stat<int> AttackChance
        {
            get { return _attackChance; }
            set { _attackChance = value; }
        }

        public Stat<int> Awareness
        {
            get { return _awareness; }
            set { _awareness = value; }
        }

        public Stat<int> Defense
        {
            get { return _defense; }
            set { _defense = value; }
        }

        public Stat<int> DefenseChance
        {
            get { return _defenseChance; }
            set { _defenseChance = value; }
        }

        public Stat<int> Gold
        {
            get { return _gold; }
            set { _gold = value; }
        }

        public Stat<int> Health
        {
            get { return _health; }
            set { _health = value; }
        }

        public Stat<int> MaxHealth
        {
            get { return _maxHealth; }
            set { _maxHealth = value; }
        }

        public Stat<int> Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public void Render(RLConsole console, IMap map)
        {
            // Don't draw actors in cells that haven't been explored
            if (!map.GetCell(X, Y).IsExplored)
            {
                return;
            }

            // Only draw the actor with the color and symbol when they are in field-of-view
            if (map.IsInFov(X, Y))
            {
                console.Set(X, Y, Color, Colors.FloorBackgroundFov, Symbol);
            }
            else
            {
                // When not in field-of-view just draw a normal floor
                console.Set(X, Y, Colors.Floor, Colors.FloorBackground, '.');
            }
        }

        public RLColor Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public char Symbol
        {
            get { return _symbol; }
            set { _symbol = value; }
        }

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }
    }
}