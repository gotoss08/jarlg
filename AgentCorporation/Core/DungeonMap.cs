﻿using RLNET;
using RogueSharp;
using RogueSharp.MapCreation;
using System.Collections.Generic;

namespace AgentCorporation.Core
{
    public class DungeonMap : Map
    {
        private List<Rectangle> _rooms;
        private readonly List<Monster> _monsters;

        public DungeonMap()
        {
            _monsters = new List<Monster>();
            _rooms = new List<Rectangle>();
        }

        public void Render(RLConsole mapConsole)
        {
            mapConsole.Clear();
            foreach (var cell in GetAllCells())
            {
                SetConsoleSymbolForCell(mapConsole, cell);
            }
            foreach (var monster in _monsters)
            {
                monster.Render(mapConsole, this);
            }
        }

        public void AddPlayer(Player player)
        {
            Engine.GameGameState.Player = player;
            SetIsWalkable(player.X, player.Y, false);
            UpdatePlayerFOV();
        }

        public void UpdatePlayerFOV()
        {
            var player = Engine.GameGameState.Player;
            ComputeFov(player.X, player.Y, player.Awareness.GetValue, true);

            foreach (var cell in GetAllCells())
            {
                if (IsInFov(cell.X, cell.Y))
                {
                    SetCellProperties(cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true);
                }
            }
        }

        public void AddMonster(Monster monster)
        {
            _monsters.Add(monster);
            SetIsWalkable(monster.X, monster.Y, false);
        }

        public bool SetActorPosition(Actor actor, int x, int y)
        {
            if (GetCell(x, y).IsWalkable)
            {
                SetIsWalkable(actor.X, actor.Y, true);

                actor.X = x;
                actor.Y = y;

                SetIsWalkable(actor.X, actor.Y, false);

                if (actor is Player)
                {
                    UpdatePlayerFOV();
                }
                return true;
            }
            return false;
        }

        public void SetIsWalkable(int x, int y, bool isWalkable)
        {
            var cell = GetCell(x, y);
            SetCellProperties(x, y, cell.IsTransparent, isWalkable, cell.IsExplored);
        }

        private void SetConsoleSymbolForCell(RLConsole console, Cell cell)
        {
            if (!cell.IsExplored)
            {
                return;
            }

            if (IsInFov(cell.X, cell.Y))
            {
                if (cell.IsWalkable)
                {
                    console.Set(cell.X, cell.Y, Colors.FloorFov, Colors.FloorBackgroundFov, '.');
                }
                else
                {
                    console.Set(cell.X, cell.Y, Colors.WallFov, Colors.WallBackgroundFov, '+');
                }
            }
            else
            {
                if (cell.IsWalkable)
                {
                    console.Set(cell.X, cell.Y, Colors.Floor, Colors.FloorBackground, '.');
                }
                else
                {
                    console.Set(cell.X, cell.Y, Colors.Wall, Colors.WallBackground, '#');
                }
            }
        }

        public Point GetRandomWalkableLocationInRoom(Rectangle room)
        {
            if (DoesRoomHaveWalkableSpace(room))
            {
                for (int i = 0; i < 100; i++)
                {
                    int x = Engine.GameGameState.Random.Next(1, room.Width - 2) + room.X;
                    int y = Engine.GameGameState.Random.Next(1, room.Height - 2) + room.Y;
                    if (IsWalkable(x, y))
                    {
                        return new Point(x, y);
                    }
                }
            }

            // If we didn't find a walkable location in the room return null
            return Point.Zero;
        }

        public bool DoesRoomHaveWalkableSpace(Rectangle room)
        {
            for (int x = 1; x <= room.Width - 2; x++)
            {
                for (int y = 1; y <= room.Height - 2; y++)
                {
                    if (IsWalkable(x + room.X, y + room.Y))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<Rectangle> Rooms
        {
            get { return _rooms; }
            set { _rooms = value; }
        }
    }
}