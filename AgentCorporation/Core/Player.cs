﻿using AgentCorporation.Structs;
using RLNET;

namespace AgentCorporation.Core
{
    public class Player : Actor
    {
        public Player()
        {
            Attack = new Stat<int>("Attack", 10);
            AttackChance = new Stat<int>("Chance of attack", 100);
            Awareness = new Stat<int>("Awareness", 10);
            Defense = new Stat<int>("Defence", 0);
            DefenseChance = new Stat<int>("Chance of defence", 0);
            Gold = new Stat<int>("Gold", 0, RLColor.Yellow);
            Health = new Stat<int>("Health", 30, RLColor.LightRed);
            MaxHealth = new Stat<int>("Max health", 30);
            Name = new Stat<string>("Name", "Player");
            Speed = new Stat<int>("Speed", 1);
            Color = Colors.Player;
            Symbol = '@';
            X = 5;
            Y = 5;
        }

        public void RenderStats(RLConsole console)
        {
            console.Print(1, 1, $"{Name.GetName}: {Name.GetValue}", Name.GetColor);
            console.Print(1, 2, $"{Health.GetName}: {Health.GetValue}/{MaxHealth.GetValue}", Health.GetColor);
            console.Print(1, 3, $"{Attack.GetName}: {Attack.GetValue} ({AttackChance.GetValue}%)", Attack.GetColor);
            console.Print(1, 4, $"{Defense.GetName}: {Defense.GetValue} ({DefenseChance.GetValue}%)", Defense.GetColor);
            console.Print(1, 5, $"{Speed.GetName}: {Speed.GetValue}", Speed.GetColor);
            console.Print(1, 6, $"{Gold.GetName}: {Gold.GetValue}", Gold.GetColor);
        }
    }
}