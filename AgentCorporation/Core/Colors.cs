﻿using RLNET;

namespace AgentCorporation.Core
{
    public static class Colors
    {
        public static RLColor Player = Swatch.Light;

        public static RLColor KoboldColor = Swatch.BrightWood;
        public static RLColor FloorBackground = RLColor.Black;

        public static RLColor Floor = Swatch.AlternateDarkest;
        public static RLColor FloorBackgroundFov = Swatch.Dark;
        public static RLColor FloorFov = Swatch.Alternate;

        public static RLColor WallBackground = Swatch.SecondaryDarkest;
        public static RLColor Wall = Swatch.Secondary;
        public static RLColor WallBackgroundFov = Swatch.SecondaryDarker;
        public static RLColor WallFov = Swatch.SecondaryLighter;

        public static RLColor TextHeading = Swatch.Light;
    }

    public static class Swatch
    {
        public static RLColor PrimaryLightest = new RLColor(118, 121, 128);
        public static RLColor PrimaryLighter = new RLColor(95, 98, 107);
        public static RLColor Primary = new RLColor(74, 77, 84);
        public static RLColor PrimaryDarker = new RLColor(52, 56, 65);
        public static RLColor PrimaryDarkest = new RLColor(32, 37, 48);

        public static RLColor SecondaryLightest = new RLColor(190, 185, 174);
        public static RLColor SecondaryLighter = new RLColor(158, 152, 139);
        public static RLColor Secondary = new RLColor(80, 74, 87);
        public static RLColor SecondaryDarker = new RLColor(97, 90, 75);
        public static RLColor SecondaryDarkest = new RLColor(71, 63, 45);

        public static RLColor AlternateLightest = new RLColor(190, 184, 174);
        public static RLColor AlternateLighter = new RLColor(158, 151, 138);
        public static RLColor Alternate = new RLColor(129, 127, 107);
        public static RLColor AlternateDarker = new RLColor(97, 95, 75);
        public static RLColor AlternateDarkest = new RLColor(71, 69, 45);

        public static RLColor ComplimentLightest = new RLColor(190, 185, 174);
        public static RLColor ComplimentLighter = new RLColor(158, 152, 139);
        public static RLColor Compliment = new RLColor(129, 122, 107);
        public static RLColor ComplimentDarker = new RLColor(97, 90, 75);
        public static RLColor ComplimentDarkest = new RLColor(71, 63, 45);

        // http://pixeljoint.com/forum/forum_posts.asp?TID=12795

        public static RLColor Dark = new RLColor(20, 12, 28);
        public static RLColor OldBlood = new RLColor(68, 36, 52);
        public static RLColor DeepWater = new RLColor(48, 52, 109);
        public static RLColor OldStone = new RLColor(78, 74, 78);
        public static RLColor Wood = new RLColor(133, 76, 48);
        public static RLColor Vegetation = new RLColor(52, 101, 36);
        public static RLColor Blood = new RLColor(208, 70, 72);
        public static RLColor Stone = new RLColor(117, 113, 97);
        public static RLColor Water = new RLColor(89, 125, 206);
        public static RLColor BrightWood = new RLColor(210, 125, 44);
        public static RLColor Metal = new RLColor(133, 149, 161);
        public static RLColor Grass = new RLColor(109, 170, 44);
        public static RLColor Skin = new RLColor(210, 170, 153);
        public static RLColor Sky = new RLColor(109, 194, 202);
        public static RLColor Sun = new RLColor(218, 212, 94);
        public static RLColor Light = new RLColor(222, 238, 214);
    }
}