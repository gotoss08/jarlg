﻿using AgentCorporation.Core;
using AgentCorporation.Structs;
using RogueSharp.DiceNotation;

namespace AgentCorporation.Monsters
{
    public class Kobold : Monster
    {
        public static Kobold Create(int level)
        {
            int health = Dice.Roll("2D5");
            return new Kobold
            {
                Attack = new Stat<int>(Dice.Roll("1D3") + level / 3),
                AttackChance = new Stat<int>(Dice.Roll("25D3")),
                Awareness = new Stat<int>(10),
                Color = Colors.KoboldColor,
                Defense = new Stat<int>(Dice.Roll("1D3") + level / 3),
                DefenseChance = new Stat<int>(Dice.Roll("10D4")),
                Gold = new Stat<int>(Dice.Roll("5D5")),
                Health = new Stat<int>(health),
                MaxHealth = new Stat<int>(health),
                Name = new Stat<string>("Kobold"),
                Speed = new Stat<int>(14),
                Symbol = 'k'
            };
        }
    }
}