﻿using AgentCorporation.Core;
using AgentCorporation.GameStates;
using AgentCorporation.Interfaces;
using RLNET;
using RogueSharp;
using RogueSharp.MapCreation;

namespace AgentCorporation
{
    public static class Engine
    {
        private static void Main(string[] args)
        {
            EngineInit();

            RlRootConsole.Update += (sender, eventArgs) =>
            {
                RlRootConsole.Title = $"Agents {Fps.GetFps(eventArgs.Time)} fps";
                RlKeyPress = RlRootConsole.Keyboard.GetKeyPress();
                CurrentGameState.Update();
            };

            RlRootConsole.Render += (sender, eventArgs) =>
            {
                RlRootConsole.Clear();
                CurrentGameState.Render();
                RlRootConsole.Draw();
            };
            RlRootConsole.Run(60);
        }

        private static void EngineInit()
        {
            RlRootConsole = new RLRootConsole("ascii_8x8.png", ScreenWidth, ScreenHeight, 8, 8, 2, "Console");
            RlRootConsole.CursorVisible = true;
            InitiateGameState(MenuGameState = new MenuGameState());
        }

        public static void InitiateGameState(IGameState gameState)
        {
            CurrentGameState = gameState;
            CurrentGameState.Init();
        }

        public static void ChangeGameState(IGameState gameState)
        {
            CurrentGameState = gameState;
        }

        public static bool GetKeyPress(RLKey key)
        {
            if (RlKeyPress != null && RlKeyPress.Key == key)
            {
                return true;
            }
            return false;
        }

        public static int ScreenWidth { get; set; } = 80;
        public static int ScreenHeight { get; set; } = 60;
        public static RLRootConsole RlRootConsole { get; set; }
        public static RLKeyPress RlKeyPress { get; set; }
        public static IGameState CurrentGameState { get; set; }
        public static MenuGameState MenuGameState { get; set; }
        public static GameProcessGameState GameGameState { get; set; }
        public static PauseGameState PauseGameState { get; set; }
    }
}