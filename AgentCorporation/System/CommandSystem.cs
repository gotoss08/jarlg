﻿using AgentCorporation.Core;

namespace AgentCorporation.System
{
    public class CommandSystem
    {
        public bool MovePlayer(Direction direction)
        {
            var x = Engine.GameGameState.Player.X;
            var y = Engine.GameGameState.Player.Y;

            switch (direction)
            {
                case Direction.DownLeft:
                    y = Engine.GameGameState.Player.Y + 1;
                    x = Engine.GameGameState.Player.X - 1;
                    break;

                case Direction.Down:
                    y = Engine.GameGameState.Player.Y + 1;
                    break;

                case Direction.DownRight:
                    y = Engine.GameGameState.Player.Y + 1;
                    x = Engine.GameGameState.Player.X + 1;
                    break;

                case Direction.Left:
                    x = Engine.GameGameState.Player.X - 1;
                    break;

                case Direction.Center:
                    break;

                case Direction.Right:
                    x = Engine.GameGameState.Player.X + 1;
                    break;

                case Direction.UpLeft:
                    y = Engine.GameGameState.Player.Y - 1;
                    x = Engine.GameGameState.Player.X - 1;
                    break;

                case Direction.Up:
                    y = Engine.GameGameState.Player.Y - 1;
                    break;

                case Direction.UpRight:
                    y = Engine.GameGameState.Player.Y - 1;
                    x = Engine.GameGameState.Player.X + 1;
                    break;

                default:
                    return false;
            }

            if (Engine.GameGameState.DungeonMap.SetActorPosition(Engine.GameGameState.Player, x, y))
                return true;
            return false;
        }
    }
}