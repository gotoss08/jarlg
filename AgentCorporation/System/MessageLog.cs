﻿using AgentCorporation.Structs;
using RLNET;
using System.Collections.Generic;

namespace AgentCorporation.System
{
    public class MessageLog
    {
        private static readonly int _maxMessages = 7;

        private readonly Queue<Message> _messages;

        public MessageLog()
        {
            _messages = new Queue<Message>();
        }

        public void Add(Message message)
        {
            _messages.Enqueue(message);

            if (_messages.Count > _maxMessages)
            {
                _messages.Dequeue();
            }
        }

        public void Add(string message)
        {
            _messages.Enqueue(new Message(message));

            if (_messages.Count > _maxMessages)
            {
                _messages.Dequeue();
            }
        }

        public void Add(string message, RLColor color)
        {
            _messages.Enqueue(new Message(message, color));

            if (_messages.Count > _maxMessages)
            {
                _messages.Dequeue();
            }
        }

        public void Render(RLConsole console)
        {
            console.Clear();
            var lines = _messages.ToArray();
            for (var i = 0; i < lines.Length; i++)
            {
                console.Print(1, i + 1, lines[i].TextMessage, lines[i].Color);
            }
        }
    }
}