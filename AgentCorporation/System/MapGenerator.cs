﻿using AgentCorporation.Core;
using AgentCorporation.Monsters;
using RLNET;
using RogueSharp;
using RogueSharp.DiceNotation;
using System;
using System.Linq;

namespace AgentCorporation.System
{
    public class MapGenerator
    {
        private readonly int _height;

        private readonly DungeonMap _map;
        private readonly int _maxRooms;
        private readonly int _roomMaxSize;
        private readonly int _roomMinSize;
        private readonly int _width;

        public MapGenerator(int width, int height, int maxRooms, int roomMaxSize, int roomMinSize)
        {
            _width = width;
            _height = height;
            _maxRooms = maxRooms;
            _roomMaxSize = roomMaxSize;
            _roomMinSize = roomMinSize;
            _map = new DungeonMap();
        }

        public DungeonMap CreateMap()
        {
            _map.Initialize(_width, _height);

            for (var r = _maxRooms; r > 0; r--)
            {
                var roomWidth = Engine.GameGameState.Random.Next(_roomMinSize, _roomMaxSize);
                var roomHeight = Engine.GameGameState.Random.Next(_roomMinSize, _roomMaxSize);
                var roomXPosition = Engine.GameGameState.Random.Next(0, _width - roomWidth - 1);
                var roomYPosition = Engine.GameGameState.Random.Next(0, _height - roomHeight - 1);

                var newRoom = new Rectangle(roomXPosition, roomYPosition, roomWidth, roomHeight);

                var newRoomIntersects = _map.Rooms.Any(room => newRoom.Intersects(room));

                if (!newRoomIntersects)
                {
                    _map.Rooms.Add(newRoom);
                }
            }

            for (var r = 0; r < _map.Rooms.Count; r++)
            {
                // Don't do anything with the first room
                if (r == 0)
                {
                    continue;
                }
                // For all remaing rooms get the center of the room and the previous room
                var previousRoomCenterX = _map.Rooms[r - 1].Center.X;
                var previousRoomCenterY = _map.Rooms[r - 1].Center.Y;
                var currentRoomCenterX = _map.Rooms[r].Center.X;
                var currentRoomCenterY = _map.Rooms[r].Center.Y;

                // Give a 50/50 chance of which 'L' shaped connecting cooridors to make
                if (Engine.GameGameState.Random.Next(1, 2) == 1)
                {
                    CreateHorizontalTunnel(previousRoomCenterX, currentRoomCenterX, previousRoomCenterY);
                    CreateVerticalTunnel(previousRoomCenterY, currentRoomCenterY, currentRoomCenterX);
                }
                else
                {
                    CreateVerticalTunnel(previousRoomCenterY, currentRoomCenterY, previousRoomCenterX);
                    CreateHorizontalTunnel(previousRoomCenterX, currentRoomCenterX, currentRoomCenterY);
                }
            }

            foreach (var room in _map.Rooms)
            {
                CreateRoom(room);
            }

            PlacePlayer();
            PlaceMonsters();

            return _map;
        }

        private void CreateRoom(Rectangle room)
        {
            for (var x = room.Left + 1; x < room.Right; x++)
            {
                for (var y = room.Top + 1; y < room.Bottom; y++)
                {
                    _map.SetCellProperties(x, y, true, true);
                }
            }
        }

        private void PlacePlayer()
        {
            var player = Engine.GameGameState.Player;
            if (player == null)
            {
                player = new Player();
            }

            player.X = _map.Rooms[0].Center.X;
            player.Y = _map.Rooms[0].Center.Y;

            _map.AddPlayer(player);
        }

        private void PlaceMonsters()
        {
            foreach (var room in _map.Rooms)
            {
                if (Dice.Roll("1D10") < 7)
                {
                    var numberOfMonsters = Dice.Roll("1D4");
                    for (int i = 0; i < numberOfMonsters; i++)
                    {
                        Point randomRoomLocation = _map.GetRandomWalkableLocationInRoom(room);

                        if (randomRoomLocation != Point.Zero)
                        {
                            var monster = Kobold.Create(1);
                            monster.X = randomRoomLocation.X;
                            monster.Y = randomRoomLocation.Y;
                            _map.AddMonster(monster);
                        }
                    }
                }
            }
        }

        private void CreateHorizontalTunnel(int xStart, int xEnd, int yPosition)
        {
            for (var x = Math.Min(xStart, xEnd); x <= Math.Max(xStart, xEnd); x++)
            {
                _map.SetCellProperties(x, yPosition, true, true);
            }
        }

        // Carve a tunnel out of the map parallel to the y-axis
        private void CreateVerticalTunnel(int yStart, int yEnd, int xPosition)
        {
            for (var y = Math.Min(yStart, yEnd); y <= Math.Max(yStart, yEnd); y++)
            {
                _map.SetCellProperties(xPosition, y, true, true);
            }
        }
    }
}