﻿using AgentCorporation.Gui;
using AgentCorporation.Interfaces;
using RLNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCorporation.GameStates
{
    public class PauseGameState : IGameState
    {
        private static GuiButtonScreen _menuGuiWindow;

        public void Init()
        {
            _menuGuiWindow = new GuiButtonScreen(Engine.RlRootConsole);
            _menuGuiWindow.AddButton(new GuiButton("Continue", RLColor.White,
                () => Engine.ChangeGameState(Engine.GameGameState)));
            _menuGuiWindow.AddButton(new GuiButton("Regenerate level", RLColor.White,
                () => Engine.InitiateGameState(Engine.GameGameState = new GameProcessGameState())));
            _menuGuiWindow.AddButton(new GuiButton("Exit", RLColor.White, () => Engine.RlRootConsole.Close()));
            _menuGuiWindow.Init();
        }

        public void Update()
        {
            _menuGuiWindow.Update(Engine.RlKeyPress);

            if (Engine.RlKeyPress != null && Engine.RlKeyPress.Key == RLKey.Escape)
            {
                Engine.ChangeGameState(Engine.GameGameState);
            }
        }

        public void Render()
        {
            _menuGuiWindow.Render();
        }
    }
}