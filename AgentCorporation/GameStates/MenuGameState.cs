﻿using AgentCorporation.Gui;
using AgentCorporation.Interfaces;
using RLNET;

namespace AgentCorporation.GameStates
{
    public class MenuGameState : IGameState
    {
        private static GuiButtonScreen _menuGuiButtonScreen;

        public void Init()
        {
            _menuGuiButtonScreen = new GuiButtonScreen(Engine.RlRootConsole);
            _menuGuiButtonScreen.AddButton(new GuiButton("Start", RLColor.White,
                () => Engine.InitiateGameState(Engine.GameGameState = new GameProcessGameState())));
            _menuGuiButtonScreen.AddButton(new GuiButton("Exit", RLColor.White, () => Engine.RlRootConsole.Close()));
            _menuGuiButtonScreen.Init();
        }

        public void Update()
        {
            _menuGuiButtonScreen.Update(Engine.RlKeyPress);
        }

        public void Render()
        {
            _menuGuiButtonScreen.Render();
        }
    }
}