﻿using AgentCorporation.Core;
using AgentCorporation.Gui;
using AgentCorporation.Interfaces;
using AgentCorporation.System;
using RLNET;
using RogueSharp;
using RogueSharp.Random;
using System;

namespace AgentCorporation.GameStates
{
    public class GameProcessGameState : IGameState
    {
        public GuiHandler GuiHandler { get; set; }
        public GuiButtonHandler GuiButtonHandler { get; set; }

        public void Init()
        {
            MessageConsole = new RLConsole(MessageConsoleWidth, MessageConsoleHeight);
            MapConsole = new RLConsole(MapConsoleWidth, MapConsoleHeight);

            //Sub systems initialization
            Seed = (int)DateTime.UtcNow.Ticks;
            Random = new DotNetRandom(Seed);

            //Message systems initialization
            MessageLog = new MessageLog();
            MessageLog.Add($"Level created with seed '{Seed}'");

            //Actors initialization
            Player = new Player();
            CommandSystem = new CommandSystem();

            //World initialization
            Generator = new MapGenerator(MapConsoleWidth, MapConsoleHeight, 20, 13, 7);
            DungeonMap = Generator.CreateMap();
            DungeonMap.UpdatePlayerFOV();

            //GUI initialization
            GuiHandler = new GuiHandler();
            GuiHandler.AddGuiWindow(10, 10, RLKey.I);
            GuiHandler.AddGuiWindow(0, 0, 10, 2, RLKey.C);
            GuiButtonHandler = new GuiButtonHandler(GuiHandler.GuiWindows[0].RlConsole);
            GuiButtonHandler.AddButton(new GuiButton("use", "Use", RLColor.White, () => { Console.WriteLine($"Gui button clicked!"); }));
            GuiButtonHandler.AddButton(new GuiButton("examine", "Examine", RLColor.White, () => { Console.WriteLine($"Gui button clicked!"); }));
            GuiButtonHandler.AddButton(new GuiButton("drop", "Drop", RLColor.White, () => { Console.WriteLine($"Gui button clicked!"); }));
        }

        public void Update()
        {
            //Gui stuff...
            GuiHandler.Update();
            GuiButtonHandler.Update();

            if (Engine.RlKeyPress != null)
            {
                if (GuiHandler.CanMove() == true)
                {
                    if (Engine.RlKeyPress.Key == RLKey.Keypad1)
                    {
                        CommandSystem.MovePlayer(Direction.DownLeft);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad2)
                    {
                        CommandSystem.MovePlayer(Direction.Down);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad3)
                    {
                        CommandSystem.MovePlayer(Direction.DownRight);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad4)
                    {
                        CommandSystem.MovePlayer(Direction.Left);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad5)
                    {
                        CommandSystem.MovePlayer(Direction.Center);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad6)
                    {
                        CommandSystem.MovePlayer(Direction.Right);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad7)
                    {
                        CommandSystem.MovePlayer(Direction.UpLeft);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad8)
                    {
                        CommandSystem.MovePlayer(Direction.Up);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.Keypad9)
                    {
                        CommandSystem.MovePlayer(Direction.UpRight);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.W)
                    {
                        CommandSystem.MovePlayer(Direction.Up);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.S)
                    {
                        CommandSystem.MovePlayer(Direction.Down);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.D)
                    {
                        CommandSystem.MovePlayer(Direction.Right);
                    }
                    else if (Engine.RlKeyPress.Key == RLKey.A)
                    {
                        CommandSystem.MovePlayer(Direction.Left);
                    }
                }
                if (Engine.RlKeyPress.Key == RLKey.Escape)
                {
                    Engine.InitiateGameState(Engine.PauseGameState = new PauseGameState());
                }
            }
        }

        public void Render()
        {
            DungeonMap.Render(MapConsole);
            Player.Render(MapConsole, DungeonMap);

            //_player.RenderStats(_statsConsole);

            //Отрисовка основных консолей, которые не наслаиваются друг на друга
            MessageLog.Render(MessageConsole);

            BlitConsoles();
            //Extra consoles to render up all other
            //Отрисовка дополнительных консолей, поверх всех остальных
            GuiHandler.Render();
            GuiButtonHandler.Render();
        }

        private void BlitConsoles()
        {
            RLConsole.Blit(MapConsole, 0, 0, MapConsoleWidth, MapConsoleHeight, Engine.RlRootConsole, 0, 0);
            RLConsole.Blit(MessageConsole, 0, 0, MessageConsoleWidth, MessageConsoleHeight, Engine.RlRootConsole, 0,
                MapConsoleHeight);
        }

        public int MapConsoleWidth { get; set; } = 80;

        public int MapConsoleHeight { get; set; } = 50;

        public RLConsole MapConsole { get; set; }

        public int MessageConsoleWidth { get; set; } = 60;

        public int MessageConsoleHeight { get; set; } = 10;

        public RLConsole MessageConsole { get; set; }

        public Player Player { get; set; }

        public IRandom Random { get; set; }

        public MapGenerator Generator { get; set; }

        public DungeonMap DungeonMap { get; set; }

        public CommandSystem CommandSystem { get; set; }

        public MessageLog MessageLog { get; set; }

        public int Seed { get; set; }
    }
}