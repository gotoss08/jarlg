﻿using RLNET;
using RogueSharp;

namespace AgentCorporation.Interfaces
{
    public interface IDrawable
    {
        void Render(RLConsole console, IMap map);

        RLColor Color { get; set; }
        char Symbol { get; set; }
        int X { get; set; }
        int Y { get; set; }
    }
}