﻿namespace AgentCorporation.Interfaces
{
    public interface IKeyHandler
    {
        void HandleInput();
    }
}