﻿using AgentCorporation.Structs;

namespace AgentCorporation.Interfaces
{
    public interface IActor
    {
        Stat<string> Name { get; set; }
        Stat<int> Attack { get; set; }
        Stat<int> AttackChance { get; set; }
        Stat<int> Awareness { get; set; }
        Stat<int> Defense { get; set; }
        Stat<int> DefenseChance { get; set; }
        Stat<int> Gold { get; set; }
        Stat<int> Health { get; set; }
        Stat<int> MaxHealth { get; set; }
        Stat<int> Speed { get; set; }
    }
}