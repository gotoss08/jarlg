﻿namespace AgentCorporation.Interfaces
{
    public interface IGameState
    {
        void Init();

        void Update();

        void Render();
    }
}