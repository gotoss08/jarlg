﻿using AgentCorporation.Interfaces;
using RLNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgentCorporation.Gui
{
    public class GuiWindow
    {
        public int X { get; set; }
        public int Y { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        private RLRootConsole RlRootConsole { get; set; }

        public RLConsole RlConsole { get; set; }

        public RLKey RlKey { get; set; }

        public bool IsActive { get; set; } = true;

        public GuiWindow()
        {
            X = 0;
            Y = 0;
            Width = Engine.ScreenWidth;
            Height = Engine.ScreenHeight;
            RlRootConsole = Engine.RlRootConsole;
            RlKey = RLKey.Tab;
        }

        public GuiWindow(RLKey key)
        {
            X = 0;
            Y = 0;
            Width = Engine.ScreenWidth;
            Height = Engine.ScreenHeight;
            RlRootConsole = Engine.RlRootConsole;
            RlKey = key;
        }

        public GuiWindow(int width, int height, RLKey key)
        {
            Width = width;
            Height = height;
            X = (Engine.RlRootConsole.Width / 2) - (width / 2);
            Y = (Engine.RlRootConsole.Height / 2) - (height / 2);
            RlRootConsole = Engine.RlRootConsole;
            RlKey = key;
        }

        public GuiWindow(int x, int y, int width, int height, RLKey key)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            RlRootConsole = Engine.RlRootConsole;
            RlKey = key;
        }

        public void Init()
        {
            if (IsActive)
            {
                RlConsole = new RLConsole(Width, Height);
            }
        }

        public void Resize()
        {
            if (IsActive)
            {
                RlConsole = new RLConsole(Width, Height);
            }
        }

        public void Update()
        {
            if (IsActive)
            {
            }
        }

        public void Render()
        {
            if (IsActive)
            {
                BlitConsole();
            }
        }

        private void BlitConsole()
        {
            RLConsole.Blit(RlConsole, 0, 0, Width, Height, RlRootConsole, X, Y);
        }

        public void SetActive(bool active)
        {
            IsActive = active;
        }

        public void ToggleActive()
        {
            IsActive = !IsActive;
        }
    }
}