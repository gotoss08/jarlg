﻿using RLNET;
using System.Collections.Generic;

namespace AgentCorporation.Gui
{
    public class GuiButtonSelectionHandler
    {
        private int _selectedId;

        private GuiButton _selectedGuiButton;

        public GuiButtonSelectionHandler()
        {
            _selectedId = 1;
            SelectedGuiButton = new GuiButton();
        }

        public void Update(List<GuiButton> buttons, RLKeyPress keyPress)
        {
            if (keyPress != null)
            {
                switch (keyPress.Key)
                {
                    case RLKey.Up:
                        _selectedId--;
                        break;

                    case RLKey.Down:
                        _selectedId++;
                        break;

                    case RLKey.Enter:
                        _selectedGuiButton.ButtonAction();
                        break;
                }
            }

            if (_selectedId > buttons.Count)
            {
                _selectedId = 1;
            }
            if (_selectedId < 1)
            {
                _selectedId = buttons.Count;
            }

            if (_selectedGuiButton != null && _selectedGuiButton != buttons[_selectedId - 1])
            {
                _selectedGuiButton.Color = _selectedGuiButton.DefaultColor;
                _selectedGuiButton = buttons[_selectedId - 1];
                _selectedGuiButton.Color = RLColor.Green;
            }
        }

        public int SelectedId
        {
            get { return _selectedId; }
            set { _selectedId = value; }
        }

        public GuiButton SelectedGuiButton
        {
            get { return _selectedGuiButton; }
            set { _selectedGuiButton = value; }
        }
    }
}