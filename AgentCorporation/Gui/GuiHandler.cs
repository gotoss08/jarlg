﻿using RLNET;
using System.Collections.Generic;
using System.Linq;

namespace AgentCorporation.Gui
{
    public class GuiHandler
    {
        public List<GuiWindow> GuiWindows { get; set; }

        private bool _needUpdateMovement;

        public GuiHandler()
        {
            GuiWindows = new List<GuiWindow>();
        }

        public void AddGuiWindow(RLKey key)
        {
            var gui = new GuiWindow(key);
            gui.Init();
            gui.SetActive(false);
            GuiWindows.Add(gui);
        }

        public void AddGuiWindow(int width, int height, RLKey key)
        {
            var gui = new GuiWindow(width, height, key);
            gui.Init();
            gui.SetActive(false);
            GuiWindows.Add(gui);
        }

        public void AddGuiWindow(int x, int y, int width, int height, RLKey key)
        {
            var gui = new GuiWindow(x, y, width, height, key);
            gui.Init();
            gui.SetActive(false);
            GuiWindows.Add(gui);
        }

        public void Update()
        {
            foreach (var guiWindow in GuiWindows)
            {
                if (Engine.RlKeyPress != null && Engine.RlKeyPress.Key == guiWindow.RlKey)
                {
                    guiWindow.ToggleActive();
                }

                guiWindow.Update();
            }

            _needUpdateMovement = true;

            if (GuiWindows.Any(window => window.IsActive))
            {
                _needUpdateMovement = false;
            }
        }

        public void Render()
        {
            foreach (var guiWindow in GuiWindows)
            {
                guiWindow.Render();
            }
        }

        public bool CanMove()
        {
            return _needUpdateMovement;
        }
    }
}