﻿using RLNET;
using System.Collections.Generic;
using System.Linq;

namespace AgentCorporation.Gui
{
    public class GuiButtonScreen
    {
        private int _width;
        private int _height;

        private readonly RLRootConsole _rlRootConsole;

        private GuiButtonSelectionHandler _selectionHandler;

        public GuiButtonScreen(RLRootConsole rlRootConsole)
        {
            GuiButtons = new List<GuiButton>();
            _rlRootConsole = rlRootConsole;
            _selectionHandler = new GuiButtonSelectionHandler();
        }

        public void Init()
        {
            CalculateWindowSize();
            RlConsole = new RLConsole(_width, _height);
        }

        private void CalculateWindowSize()
        {
            GuiButtons.ForEach(button =>
            {
                if (button.Text.Length > _width)
                {
                    _width = button.Text.Length;
                }
            });

            _height = GuiButtons.Count;
        }

        public void Update(RLKeyPress keyPress)
        {
            _selectionHandler.Update(GuiButtons, Engine.RlKeyPress);
        }

        public void Render()
        {
            for (var i = 0; i < GuiButtons.Count; i++)
            {
                RlConsole.Print(RlConsole.Width / 2 - GuiButtons[i].Text.Length / 2, i, GuiButtons[i].Text,
                    GuiButtons[i].Color);
            }

            RLConsole.Blit(RlConsole, 0, 0, RlConsole.Width, RlConsole.Height, _rlRootConsole,
                (_rlRootConsole.Width / 2 - RlConsole.Width / 2), (_rlRootConsole.Height / 2 - RlConsole.Height / 2));
        }

        public void AddButton(GuiButton button)
        {
            GuiButtons.Add(button);
        }

        public RLConsole RlConsole { get; set; }
        public List<GuiButton> GuiButtons { get; set; }
    }
}