﻿using RLNET;

namespace AgentCorporation.Gui
{
    public class GuiButton
    {
        public delegate void Action();

        private string _idName;
        private Action _buttonAction;
        private RLColor _color;
        private RLColor _defaultColor;
        private string _text;

        public GuiButton()
        {
        }

        public GuiButton(string text, RLColor color, Action buttonAction)
        {
            _idName = text;
            _text = text;
            _color = color;
            _defaultColor = color;
            _buttonAction = buttonAction;
        }

        public GuiButton(string idName, string text, RLColor color, Action buttonAction)
        {
            _idName = idName;
            _text = text;
            _color = color;
            _defaultColor = color;
            _buttonAction = buttonAction;
        }

        public string IdName
        {
            get { return _idName; }
            set { _idName = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public RLColor Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public RLColor DefaultColor
        {
            get { return _defaultColor; }
            set { _defaultColor = value; }
        }

        public Action ButtonAction
        {
            get { return _buttonAction; }
            set { _buttonAction = value; }
        }
    }
}